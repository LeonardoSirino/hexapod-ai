import numpy as np
from scipy.optimize import minimize
from scipy.spatial.transform import Rotation
import pandas as pd

from flange_orientation import flange, pointCouple

R = 100
z = 100
d_theta = np.deg2rad(15)
my_flange = flange()

for ang in np.linspace(0, 2 * np.pi, num=3):
    PC = pointCouple()
    PC.set_coords([R * np.cos(ang), R * np.sin(ang), 0,
                   R * np.cos(ang + d_theta), R * np.sin(ang + d_theta), z])
    my_flange.add_PC(PC)

    PC = pointCouple()
    PC.set_coords([R * np.cos(ang), R * np.sin(ang), 0,
                   R * np.cos(ang - d_theta), R * np.sin(ang - d_theta), z])
    my_flange.add_PC(PC)

df = pd.DataFrame(columns=['transformation', 'displacements'])

POINTS = 2000
RANGES = [50, 50, 50, np.deg2rad(30), np.deg2rad(30), np.deg2rad(30)]
RANGES = np.array(RANGES)

for _ in range(POINTS):
    x = 2 * (np.random.random(6) - 0.5)
    x *= RANGES

    m_disps = my_flange.calc_disps(x)

    data = {
        'transformation': x,
        'displacements': m_disps
    }

    df = df.append(data, ignore_index=True)

print(df)
df.to_pickle('Data/sample.pkl')
