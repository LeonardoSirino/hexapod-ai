import tensorflow as tf
from tensorflow.keras import datasets, layers, models, regularizers
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import datetime

VALIDATION_FRAC = 0.1

data_file_path = r'Data/sample.pkl'
df = pd.read_pickle(data_file_path)

y = df['transformation'].to_list()
x = df['displacements'].to_list()

y = np.array(y)
x = np.array(x)

lines, columns = np.shape(x)
split = int(lines * VALIDATION_FRAC)

x_train = x[:split, :]
y_train = y[:split, :]

x_valid = x[split:, :]
y_valid = y[split:, :]

model = tf.keras.Sequential()
model.add(layers.Dense(units=12, input_shape=[6]))

model.add(layers.Dense(units=48,
                       activation='relu',
                       ))

model.add(layers.Dense(units=6))

initial_learning_rate = 0.001
lr_schedule = tf.keras.optimizers.schedules.ExponentialDecay(
    initial_learning_rate,
    decay_steps=1000,
    decay_rate=0.8,
    staircase=False)

model.compile(optimizer=tf.keras.optimizers.SGD(learning_rate=lr_schedule),
              loss=tf.keras.losses.MeanSquaredError(),
              metrics=['accuracy'])

# model.compile(optimizer=tf.keras.optimizers.Adam(0.0001),
#               loss=tf.keras.losses.MeanSquaredError(),
#               metrics=['accuracy'])

log_dir = "logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
tensorboard_callback = tf.keras.callbacks.TensorBoard(
    log_dir=log_dir, histogram_freq=1)

history = model.fit(x_train, y_train,
                    epochs=1000,
                    verbose=True,
                    callbacks=[tensorboard_callback],
                    validation_data=(x_valid, y_valid))

model.save('model.h5')
