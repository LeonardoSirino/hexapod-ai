import tensorflow as tf
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

MODEL_PATH = r'/home/sirino/Documents/Projetos/Hexapod_AI/model.h5'

data_file_path = r'/home/sirino/Documents/Projetos/Hexapod_AI/Data/sample.pkl'
df = pd.read_pickle(data_file_path)

y = df['transformation'].to_list()
x = df['displacements'].to_list()

y = np.array(y)
x = np.array(x)

model = tf.keras.models.load_model(MODEL_PATH)

x = x[:2, :]
y_ref = y[:2, :]

print(x)
print(np.shape(x))

y_model = model.predict(x)

print('Distâncias [mm]')
print(x)
print('Orientação real [x y z alpha beta gama]')
print(y_ref)
print('Orientação calculada [x y z alpha beta gama]')
print(y_model)
