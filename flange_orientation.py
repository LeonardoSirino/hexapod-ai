import numpy as np
from scipy.spatial.transform import Rotation
from scipy.optimize import minimize
import copy


class pointCouple:
    def __init__(self):
        pass

    def set_coords(self, coords):
        self.fixed_coords = np.array(coords[0:3])
        self.movable_coords = np.array(coords[3:6])
        self.__calc_init_length()

    def __calc_init_length(self):
        self.init_length = np.linalg.norm(
            self.fixed_coords - self.movable_coords)

    def __str__(self):
        text = 'Coordenada fixa:\n'
        for coord in self.fixed_coords:
            text += str(coord) + ', '

        text = text[:-2]
        text += '\nCoordenada móvel:\n'
        for coord in self.movable_coords:
            text += str(coord) + ', '

        text = text[:-2]
        text += '\nDistância inicial: ' + str(self.init_length) + ' mm\n\n'
        return text


class flange():
    def __init__(self):
        self.PCs = []

    def add_PC(self, PC: pointCouple):
        self.PCs.append(PC)

    def calc_residue(self, x, m_disps):
        c_disps = self.calc_disps(x)
        residue = 0

        for c_disp, m_disp in zip(c_disps, m_disps):
            residue += (c_disp - m_disp) ** 2

        residue = np.sqrt(residue)

        return residue

    def calc_disps(self, x):
        deslocs = np.array(x[0:3])
        angs = x[3:6]

        r = Rotation.from_euler('xyz', angs)
        points = []
        z = []

        for PC in self.PCs:
            point = copy.copy(PC.movable_coords)
            z.append(point[2])
            point[2] = 0
            points.append(point)

        points = r.apply(points)
        points[:, 2] += z

        points += deslocs
        c_disps = []

        for point, PC in zip(points, self.PCs):
            c_dist = np.linalg.norm(point - PC.fixed_coords)
            c_disp = c_dist - PC.init_length
            """
            print(c_dist)
            print(PC.init_length)
            """
            c_disps.append(c_disp)

        return c_disps


if __name__ == '__main__':

    R = 100
    z = 100
    my_flange = flange()

    for ang in np.linspace(0, 2 * np.pi, num=6):
        PC = pointCouple()
        PC.set_coords([R * np.cos(ang), R * np.sin(ang), 0,
                       R * np.cos(ang), R * np.sin(ang), z])
        my_flange.add_PC(PC)

    x = [0, 0, 1, 0, 0, 0]
    m_disps = my_flange.calc_disps(x)

    def residue(x):
        residue = my_flange.calc_residue(x, m_disps)
        if residue < 1E-3:
            residue = 0
        return residue

    res = minimize(residue, x0=6 * [0], method='BFGS')
    print(res)
